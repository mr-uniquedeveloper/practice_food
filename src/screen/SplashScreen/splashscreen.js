import React, {useEffect} from 'react';

import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  style,
  Image,
} from 'react-native';
import {
  useNavigation,
  StackActions,
  CommonActions,
  useRoute,
} from '@react-navigation/native';
import * as Progress from 'react-native-progress';
export default function SplashScreen() {
  const navigation = useNavigation();
  const route = useRoute();

  useEffect(() => {
    // checkUserAuth();
    // }
    goToHome('Intro');
  }, []);

  const goToHome = (page, params = {}) => {
    setTimeout(() => {
      navigation.dispatch(
        CommonActions.reset({
          index: 0,
          routes: [{name: page, params: params}],
        }),
      );
    }, 1000);
  };
  return (
    <SafeAreaView style={styles.pageContainer}>
      <View style={{flex: 5, justifyContent: 'center', alignItems: 'center'}}>
        <View>
          <Image
            source={require('../../assets/images/mlogo.png')}
            style={{width: 100, maxHeight: 100}}
          />
          <Progress.Bar
            color={'#fff'}
            indeterminate={true}
            width={70}
            thickness={4}
            style={{
              position: 'absolute',
              top: 145,
              left: 15,
            }}
          />
          <Text style={styles.title}>McDelivery</Text>
        </View>
      </View>
      <View
        style={{flex: 0.9, justifyContent: 'flex-end', alignItems: 'center'}}>
        <View style={{flexDirection: 'row', paddingBottom: 10}}>
          <Text style={styles.title}>FastDelivery | </Text>
          <Text style={styles.title}> Best Offers</Text>
        </View>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  pageContainer: {
    flex: 1,
    backgroundColor: '#facc5a',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 20,
    paddingTop: 12,
  },
});
