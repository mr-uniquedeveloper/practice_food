import React, { useState, useEffect, u } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  StatusBar,
  Alert,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  FlatList,
  Button,
  Animation,
  Animated,
} from 'react-native';
import ThemeStyle from '../../styles/ThemeStyle';
import Color from '../../styles/Color';
import Icon from '../../common/Icons';
import ImageBlurLoading from 'react-native-image-blur-loading';
import CustomButton from '../../components/CustomButton/CustomButton';
import IconBadge from 'react-native-icon-badge';
import Toast from 'react-native-toast-message';
import { useNavigation } from '@react-navigation/native';
import ReactTags from 'react-tag-input';

const categories = [
  {
    image: require('../../assets/images/burger1.jpeg'),
  },
  {
    image: require('../../assets/images/bur2.jpeg'),
  },
  {
    image: require('../../assets/images/bur3.jpeg'),
  },
  {
    image: require('../../assets/images/bur4.jpeg'),
  },
];

const contents = [
  {
    name: 'Mc Veg Burger',
    image: require('../../assets/images/veg.png'),
    isVeg: true,
    price: 125,
  },
  {
    name: 'Mc Chicken Burger',
    image: require('../../assets/images/chicken.png'),
    price: 131,
  },
  {
    name: 'Mc Spicy Burger',
    image: require('../../assets/images/spicychicken.png'),
    price: 181,
  },
  {
    name: 'Fillet-o-Fish Burger',
    image: require('../../assets/images/fish.png'),
    price: 125,
  },
  {
    name: 'Mc Tikki Burger',
    image: require('../../assets/images/tikki.png'),
    isVeg: true,
    price: 55,
  },
  {
    name: 'Mc Chicken Wrap',
    image: require('../../assets/images/wrap.png'),
    price: 125,
  },
];
const snacks = [
  {
    image: require('../../assets/images/burgerwraps.png'),
    title: 'MC Burger Wrapss',
  },
  {
    image: require('../../assets/images/GourmetBurger.png'),
    title: 'MC Gourmet Burger',
  },
  {
    image: require('../../assets/images/Ez.png'),
    title: 'Ez Snack and drinks',
  },
  {
    image: require('../../assets/images/Spicy.png'),
    title: 'MC Spicy Chicken',
  },
  {
    image: require('../../assets/images/saver.png'),
    title: 'Mc Saver',
  },
  {
    image: require('../../assets/images/cola.png'),
    title: 'Beverages',
  },
  {
    image: require('../../assets/images/Happymeals.png'),
    title: 'Happy Meals',
  },
  {
    image: require('../../assets/images/sides.png'),
    title: 'Beverages',
  },
  {
    image: require('../../assets/images/dessert.png'),
    title: 'Beverages',
  },
];

export default function HomeScreen() {
  const [tags, setTags] = useState([
    { id: 'Thailand', text: 'Thailand' },
    { id: 'India', text: 'India' },
    { id: 'Vietnam', text: 'Vietnam' },
    { id: 'Turkey', text: 'Turkey' }
  ]);

  const navigation = useNavigation();
  const [shouldShow, setShouldShow] = useState(false);
  const [visible, setVisible] = useState(false);
  const badgeScale = new Animated.Value(0);
  const moveAnimation = new Animated.Value(0);
  const [num, setNum] = useState(0);
  const AnimatedHeadeValue = new Animated.Value(0);
  const Header_Max_Height = 100;
  const Header_Min_Height = 90;
  const animatedHeaderBackgroundColor = AnimatedHeadeValue.interpolate({
    inputRange: [0, Header_Max_Height - Header_Min_Height],
    outputRange: ['#facc5a', 'white'],
    extrapolate: 'clamp',
  });

  const animationHeaderHeight = AnimatedHeadeValue.interpolate({
    inputRange: [10, Header_Max_Height - Header_Min_Height],
    outputRange: [Header_Max_Height, Header_Min_Height],
    extrapolate: 'clamp',
  });
  const move = () => {
    Animated.timing(moveAnimation, {
      toValue: 300,
      timing: 1000,
      useNativeDriver: true,
    }).start();
  };
  const animatedBadge = () => {
    badgeScale.setValue(0);
    // setTimeout(() => {
    //   setVisible(!visible);
    // }, 500);

    Toast.show({
      type: 'success',
      text1: 'Card',
      text2: 'Card Added Successfully',
      autoHide: true,
      visibilityTime: 1000,
      position: 'top',
    });
    Animated.timing(badgeScale, {
      toValue: 1,
      duration: 500,
      useNativeDriver: true,
    }).start(() => setNum(Number(num) + 1));
  };
  const renderHeader = () => {
    return (
      <Animated.View
        style={[
          styles.header,
          {
            height: animationHeaderHeight,
            backgroundColor: animatedHeaderBackgroundColor,
          },
        ]}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            alignItems: 'center',
            height: '100%',
          }}>
          <Text
            style={{
              color: Color.black,
              fontSize: 18,
              fontWeight: '700',
              paddingTop: 15,
            }}>
            Delivery
          </Text>
          <Icon
            family="AntDesign"
            name={'down'}
            color={Color.black}
            size={20}
            style={{ paddingTop: 15, paddingLeft: 5 }}
          />
          <TouchableOpacity onPress={() => alert('setlocation')}>
            <Text
              style={{
                fontSize: 13,
                color: Color.gray1,
                position: 'relative',
                top: 5,
                left: 12,
              }}>
              Location not set
            </Text>
            <View style={styles.line} />
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => {
              alert('search');
            }}>
            <Icon
              family="Ionicons"
              name={'md-search-outline'}
              color={Color.brown}
              size={22}
              style={{ paddingTop: 15, paddingLeft: 90 }}
            />
          </TouchableOpacity>
          <View>
            <TouchableOpacity onPress={() => navigation.navigate('Card')}>
              <Icon
                family="FontAwesome"
                name={'shopping-cart'}
                color={animatedBadge ? Color.black : Color.darkBlue}
                size={22}
                style={{ paddingTop: 15, paddingLeft: 15 }}
              />
            </TouchableOpacity>

            <Animated.View
              style={{
                position: 'absolute',
                width: 18,
                height: 18,
                borderRadius: 20,
                backgroundColor: 'red',
                justifyContent: 'center',

                left: 28,
                top: 6,
                transform: [
                  {
                    scale: badgeScale,
                  },
                ],
              }}>
              <Text style={{ textAlign: 'center', color: 'white', fontSize: 12 }}>
                {num}
              </Text>
            </Animated.View>
          </View>
        </View>
      </Animated.View>
    );
  };
  const renderFilteredItems = () => {
    return (
      <View style={{}}>
        <FlatList
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          data={categories}
          renderItem={({ item, index }) => _renderCategory(item, index)}
        />
      </View>
    );
  };

  const _renderCategory = (item, index) => {
    const cardHeight = 180;
    return (
      <View
        style={{
          backgroundColor: Color.white,
          marginVertical: 5,
          borderRadius: 12,
          width: 320,
          shadowColor: '#171717',
          shadowOffset: { width: -2, height: 4 },
          shadowOpacity: 0.2,
          shadowRadius: 3,
          marginRight: 10,
          overflow: 'hidden',
        }}>
        <View style={{ height: cardHeight }}>
          <ImageBlurLoading
            source={item.image}
            style={{
              width: '100%',
              height: '100%',
              resizeMode: 'cover',
              borderRadius: 12,
            }}
          />
        </View>
      </View>
    );
  };
  const _renderContent = (item, index) => {
    const cardHeight = 200;

    return (
      <>
        <View style={visible ? styles.addtoCard : styles.contentCard}>
          <View style={{ height: cardHeight }}>
            <View>
              <TouchableOpacity onPress={() => setShouldShow(!shouldShow)}>
                <Icon
                  family="AntDesign"
                  name={'hearto'}
                  color={Color.gray3}
                  size={20}
                  style={shouldShow ? { display: 'none' } : styles.grayHeart}
                />
              </TouchableOpacity>
              {shouldShow ? (
                <TouchableOpacity onPress={() => setShouldShow(!shouldShow)}>
                  <Icon
                    family="AntDesign"
                    name={'heart'}
                    color={'red'}
                    size={20}
                    style={{
                      textAlign: 'right',
                      position: 'relative',
                      top: 18,
                      right: 10,
                    }}
                  />
                </TouchableOpacity>
              ) : null}
            </View>
            <ImageBlurLoading
              source={item.image}
              style={{
                width: '65%',
                height: '40%',
                marginTop: -5,
                marginLeft: 25,
                justifyContent: 'center',
                alignItems: 'center',
                resizeMode: 'cover',
                borderRadius: 12,
              }}
            />

            <View style={{ flexDirection: 'row' }}>
              <View style={{ paddingLeft: 20, paddingTop: 17 }}>
                {item.isVeg ? (
                  <View
                    style={{
                      height: 15,
                      width: 15,
                      borderWidth: 0.5,
                      borderColor: Color.lightGreen,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <View
                      style={{
                        height: 8,
                        width: 8,
                        borderRadius: 5,
                        backgroundColor: Color.lightGreen,
                      }}
                    />
                  </View>
                ) : (
                  <View
                    style={{
                      height: 15,
                      width: 15,
                      borderWidth: 0.5,
                      borderColor: 'red',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <View
                      style={{
                        height: 8,
                        width: 8,
                        borderRadius: 5,
                        backgroundColor: 'red',
                      }}
                    />
                  </View>
                )}
              </View>
              <View style={{ paddingTop: 15 }}>
                <Text
                  style={{
                    fontSize: 13,
                    fontWeight: '600',
                    textAlign: 'center',
                    paddingHorizontal: 3,
                  }}>
                  {item.name}
                </Text>
              </View>
            </View>
            <View style={{ flexDirection: 'row', marginTop: 35 }}>
              <View>
                <Text style={{ fontSize: 15 }}> {'	\u20B9'}</Text>
              </View>
              <View>
                <Text style={{ fontSize: 15, paddingBottom: 10 }}>
                  {item.price}
                </Text>
              </View>

              <CustomButton
                onPress={() => animatedBadge()}
                text="ADD"
                containerStyle={{
                  borderRadius: 10,
                  width: '35%',
                  height: 42,
                  position: 'relative',
                  bottom: 20,
                  left: 25,
                }}
                innerTextStyle={{
                  fontSize: 12,
                  color: Color.black,
                  fontWeight: '700',
                }}
              />
              <Toast />
            </View>
          </View>
        </View>
      </>
    );
  };

  const renderContentItems = () => {
    return (
      <View style={{}}>
        <View style={{ paddingTop: 25, paddingBottom: 8 }}>
          <Text style={{ fontSize: 20, fontWeight: '700' }}>Good Evening,</Text>
        </View>
        <FlatList
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          data={contents}
          renderItem={({ item, index }) => _renderContent(item, index)}
        />
      </View>
    );
  };
  const _renderSnacks = (item, index) => {
    const cardHeight = 260;
    return (
      <>
        <TouchableOpacity style={{ flex: 1 }} onPress={() => { }}>
          <View
            style={{ flex: 1, alignItems: 'center', marginBottom: 24 }}
            key={`snacks${index}`}
            onPress={() => { }}>
            <View style={{ width: '65%', height: 42 }}>
              <Image
                source={item.image}
                style={{ width: '140%', height: '140%', resizeMode: 'contain' }}
              />
            </View>
            <View
              style={{
                marginTop: 22,
                paddingLeft: 18,
                flex: 1,
              }}>
              <Text
                style={{
                  color: Color.black,
                  fontSize: 14,
                  fontStyle: 'normal',
                  fontWeight: '300',
                  textAlign: 'center',
                }}>
                {item.title}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      </>
    );
  };
  const renderSnacksItems = () => {
    return (
      <View style={{}}>
        <View style={{ paddingTop: 25, paddingBottom: 8 }}>
          <Text style={{ fontSize: 20, fontWeight: '700' }}>Time for Snacks</Text>
        </View>
        <View
          style={{
            flex: 1,
            marginVertical: 4,
            borderRadius: 15,
            width: '100%',
            backgroundColor: '#fff',
            borderColor: '#000',
            paddingTop: 20,
            height: 380,
            paddingRight: 15,
          }}>
          <FlatList
            data={snacks}
            renderItem={({ item, index }) => _renderSnacks(item, index)}
            numColumns={3}
            scrollEnabled={false}
          />
        </View>
      </View>
    );
  };
  const renderCard = () => {
    return (
      <View
        style={{
          position: 'static',
          top: 80,
          display: 'flex',
          justifyContent: 'flex-end',
          alignItems: 'flex-end',
        }}>
        <Text>Helllo</Text>
      </View>
    );
  };
  return (
    <SafeAreaView
      style={{ backgroundcolor: Color.yellow }}
      edges={['top', 'left', 'right']}>
      <StatusBar
        backgroundColor={Color.statusBarColor}
        barStyle="dark-content"
      />

      <View style={{}}>
        <View style={[styles.topheader]} />
        {renderHeader()}
        {/* <ScrollView
          showsVerticalScrollIndicator={false}
          scrollEnabled={10}
          onScroll={Animated.event(
            [{ nativeEvent: { contentOffset: { y: AnimatedHeadeValue } } }],
            { useNativeDriver: false },
          )}>
          <View
            style={{
              marginHorizontal: 24,
              paddingVertical: 8,
              justifyContent: 'center',
              marginBottom: 80,
              flex: 1,
            }}>
            {renderFilteredItems()}
            {renderContentItems()}
            <Toast />
            {renderSnacksItems()}
            {/* 
            <Button title="Add" onPress={() => move()} />
            <Text style={[styles.text, styles.moveAnimationStyle]}> HELLO</Text> */}
        {/* <Animated.View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                  transform: [
                    {
                      scale: badgeScale,
                    },
                  ],
                }}>
                <IconBadge
                  MainElement={
                    <View
                      style={{
                        backgroundColor: '#489EFE',
                        width: 50,
                        height: 50,
                        margin: 6,
                      }}
                    />
                  }
                  BadgeElement={<Text style={{color: '#FFFFFF'}}>{num}</Text>}
                  IconBadgeStyle={{
                    width: 30,
                    height: 30,
                    backgroundColor: '#FF00EE',
                  }}
                />
              </Animated.View> */}
        {/* </View>
        </ScrollView> */}
        <View>
          <ReactTags
            tags={tags}



          />
        </View>

      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  header: {
    height: 110,
    display: 'flex',
    paddingLeft: 22,
    paddingTop: 10,
    backgroundColor: Color.yellow,
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'center',
    overflow: 'hidden',
  },
  roomeTxt: {
    fontSize: 27,
    fontFamily: 'Bold',
    color: '#FFF',
  },
  imgSty: {
    width: 300,
    height: 200,
  },
  line: {
    height: 1,
    width: '180%',
    backgroundColor: Color.black,
    position: 'relative',
    top: 10,
    left: 10,
  },
  grayHeart: {
    textAlign: 'right',
    position: 'relative',
    top: 18,
    right: 10,
  },
  card: {
    width: '20%',
    backgroundColor: 'gray',
    height: 40,
    left: 130,
    top: 50,
  },
  touchable: {
    alignItems: 'center',
  },
  text: {
    color: Color.black,
    fontSize: 20,
  },
  name: {
    color: 'red',
  },
  contentCard: {
    backgroundColor: Color.white,
    marginVertical: 4,
    borderRadius: 12,
    width: 165,
    shadowColor: '#171717',
    shadowOffset: { width: -2, height: 4 },
    shadowOpacity: 0.2,
    shadowRadius: 3,
    marginRight: 15,
    overflow: 'hidden',
  },
  addtoCard: {
    backgroundColor: Color.gray1,
    marginVertical: 4,
    borderRadius: 12,
    width: 165,
    shadowColor: '#171717',
    shadowOffset: { width: -2, height: 4 },
    shadowOpacity: 0.2,
    shadowRadius: 3,
    marginRight: 15,
    overflow: 'hidden',
    color: 'white',
  },
  topheader: {
    backgroundColor: Color.yellow,
    width: '100%',
    height: 50,
    marginVertical: -48,
  },
});
