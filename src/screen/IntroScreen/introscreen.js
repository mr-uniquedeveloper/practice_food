import * as React from 'react';
import {View, Text, Button, Image, TouchableOpacity} from 'react-native';

import Onboarding from 'react-native-onboarding-swiper';

const Done = ({...props}) => (
  <TouchableOpacity style={{marginHorizonatl: 10}} {...props}>
    <Text style={{fontSize: 16, paddingRight: 15, fontWeight: '600'}}>
      Done
    </Text>
  </TouchableOpacity>
);

function IntroScreen({navigation}) {
  return (
    <Onboarding
      onSkip={() => navigation.replace('Home')}
      onDone={() => navigation.replace('Home')}
      DoneButtonComponent={Done}
      pages={[
        {
          backgroundColor: '#facc5a',
          titleStyles: {
            color: '#000',
            position: 'relative',
            bottom: 350,
            fontWeight: 'normal',
          },
          title: 'Dine In',
          subTitleStyles: {
            color: 'red',
            position: 'relative',
            fontWeight: '700',
            fontSize: 25,
            bottom: 50,
            paddingRight: 40,
            paddingLeft: 50,
          },
          subtitle: 'Order and dine at the restaurant',
          image: (
            <Image
              source={require('../../assets/images/intro4.png')}
              style={{width: 250, height: 250, position: 'relative', top: 25}}
            />
          ),
        },
        {
          backgroundColor: '#facc5a',
          titleStyles: {
            color: '#000',
            position: 'relative',
            bottom: 340,
            fontWeight: 'normal',
          },
          title: 'Delivery',
          subTitleStyles: {
            color: 'red',
            position: 'relative',
            fontWeight: '700',
            fontSize: 25,
            bottom: 50,
            paddingRight: 60,
            paddingLeft: 50,
          },
          subtitle: 'Super fast food delivery',

          image: (
            <Image
              source={require('../../assets/images/intro2.png')}
              style={{width: 250, height: 250, top: 30}}
            />
          ),
        },
        {
          backgroundColor: '#facc5a',
          titleStyles: {
            color: '#000',
            position: 'relative',
            bottom: 400,
            fontWeight: 'normal',
          },
          title: 'Pick-up',
          subTitleStyles: {
            color: 'red',
            position: 'relative',
            fontWeight: '700',
            fontSize: 25,
            bottom: 120,
            paddingRight: 60,
            paddingLeft: 80,
          },
          subtitle: 'Pick up from the store',
          image: (
            <Image
              source={require('../../assets/images/intro3.png')}
              style={{width: 350, height: 380, top: 28}}
            />
          ),
        },
      ]}
    />
  );
}
export default IntroScreen;
