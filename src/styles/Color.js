const AppColor = {
  white: '#fff',
  black: '#000',
  gray2: '#717171',
  gray1: '#4A4041',
  gray3: '#6B6B6B',
  gray4: '#5B583A',
  lightGrey2: '#adadad',
  lightGrey: '#edeff2',
  logoRed: '#E51F28',
  brownDark: '#6F4C5B',
  brown: '#9E7777',
  mediumBrown: '#BD7B07',
  lightBrown: '#B07B2C',
  transparent: 'transparent',
  green: '#178f37',
  lightGreen: '#3A9213',
  darkBlue: '#1c3078',
  lightBlue: '#d7eaff',
  skyBlue: '#a5d0ff',
  orange: '#EDAA00',
  grey5: '#B4BCBA',
  yellow: '#facc5a',
  beige: '#F5E8C7',
  beigeLight: '#FFF8E8',
  beigeLight2: '#fcfaf5',
  beigeDark: '#DEBA9D',
  brownMedium: '#EFD6B6',
};

const Color = {
  ...AppColor,
  btnColor: AppColor.darkBlue,
  bgColor: AppColor.yellow,
  fontColor: '#333333',
  statusBarColor: AppColor.yellow,
};

export default Color;
