import Color from './Color';

export default {
  mainColor: '#0E4A86',
  bgcolor: '#35c4bb',

  faqcolor: 'rgba(68, 134, 255,0.20)',
  errorcolor: 'red',

  pageContainer: {
    flex: 1,
    backgroundColor: Color.bgColor, //'#F5F9FF'
  },
  //  1767BC
};
