import React from 'react';

import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import SplashScreen from '../../screen/SplashScreen/index';
import HomeScreen from '../../screen/HomeScreen/homescreen';
import IntroScreen from '../../screen/IntroScreen';
import Card from '../Card';

const Stack = createStackNavigator();
export default function Navigation() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name=" "
          component={SplashScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Intro"
          options={{headerShown: false}}
          component={IntroScreen}
        />
        <Stack.Screen
          name="Home"
          options={{headerShown: false}}
          component={HomeScreen}
        />
        <Stack.Screen
          name="Card"
          options={{headerShown: false}}
          component={Card}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
