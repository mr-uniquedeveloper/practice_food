import {Text, StyleSheet, Pressable} from 'react-native';
import React from 'react';
import Fonts from '../../styles/Fonts';
import Color from '../../styles/Color';

const CustomButton = ({
  onPress,
  text,
  type = 'PRIMARY',
  bgColor,
  fgColor,
  containerStyle = {},
  innerTextStyle = {},
  disabled = false,
}) => {
  return (
    <Pressable
      onPress={onPress}
      style={[
        styles.container,
        styles[`container_${type}`],
        bgColor ? {backgroundColor: bgColor} : {},
        containerStyle,
      ]}
      disabled={disabled}>
      <Text
        style={[
          styles.text,
          styles[`text_${type}`],
          fgColor ? {color: fgColor} : {},
          innerTextStyle,
        ]}>
        {text}
      </Text>
    </Pressable>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    padding: 15,
    marginVertical: 5,
    alignItems: 'center',
    borderRadius: 12,
    shadowColor: '#171717',
    shadowOffset: {width: -0.5, height: 1},
    shadowOpacity: 0.2,
    shadowRadius: 3,
    marginRight: 15,
  },
  container_PRIMARY: {
    backgroundColor: Color.yellow,
  },
  container_TERTIARY: {},
  container_SECONDARY: {
    borderColor: Color.black,
    borderWidth: 1,
  },
  text: {
    // fontWeight: "bold",
    color: Color.beige,
  },
  text_TERTIARY: {
    color: 'grey',
  },
  text_SECONDARY: {
    color: Color.yellow,
  },
});

export default CustomButton;
