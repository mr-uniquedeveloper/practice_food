import React, {useEffect} from 'react';

import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  style,
  Image,
  Animated,
  Animation,
} from 'react-native';

import Icon from '../../common/Icons';
import ThemeStyle from '../../styles/ThemeStyle';
import Color from '../../styles/Color';
import * as Progress from 'react-native-progress';
import {useNavigation} from '@react-navigation/native';
import {TouchableOpacity} from 'react-native-gesture-handler';
export default function Card() {
  const DATA = [
    'Item1 ',
    'Item2',
    'Item3 ',
    'Item4',
    'Item5',
    'Item6',
    'Item7 ',
    'Item8',
    'Item9 ',
    'Item10',
  ];
  const navigation = useNavigation();
  const AnimatedHeadeValue = new Animated.Value(0);
  const Header_Max_Height = 60;
  const Header_Min_Height = 50;
  const animatedHeaderBackgroundColor = AnimatedHeadeValue.interpolate({
    inputRange: [0, Header_Max_Height - Header_Min_Height],
    outputRange: ['white', 'yellow'],
    extrapolate: 'extend',
  });
  const animationHeaderHeight = AnimatedHeadeValue.interpolate({
    inputRange: [0, Header_Max_Height - Header_Min_Height],
    outputRange: [Header_Max_Height, Header_Min_Height],
    extrapolate: 'clamp',
  });
  const goBackPress = () => {
    navigation.goBack();
  };
  return (
    <>
      <SafeAreaView style={styles.container}>
        <Animated.View
          style={[
            styles.header,
            {
              height: animationHeaderHeight,
              backgroundColor: animatedHeaderBackgroundColor,
            },
          ]}>
          <View style={{flexDirection: 'row'}}>
            <View
              style={{
                position: 'relative',
                right: 120,
              }}>
              <TouchableOpacity onPress={goBackPress}>
                <Icon
                  family="AntDesign"
                  name={'arrowleft'}
                  color={Color.black}
                  size={22}
                  style={{textAlign: 'center'}}
                />
              </TouchableOpacity>
            </View>

            <View style={{}}>
              <Text style={styles.headerText}>Add to Card</Text>
            </View>
          </View>
        </Animated.View>
        <ScrollView
          scrollEnabled={10}
          onScroll={Animated.event(
            [{nativeEvent: {contentOffset: {y: AnimatedHeadeValue}}}],
            {useNativeDriver: false},
          )}>
          {DATA.map((item, index) => (
            <Text style={styles.textStyle} key={index}>
              {item}
            </Text>
          ))}
        </ScrollView>
      </SafeAreaView>
    </>
  );
}

const styles = StyleSheet.create({
  pageContainer: {
    flex: 1,
    backgroundColor: '#facc5a',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 20,
    paddingTop: 12,
  },
  container: {flex: 1},
  textStyle: {
    textAlign: 'center',
    color: '#000',
    fontSize: 18,
    padding: 20,
  },
  header: {
    justifyContent: 'center',
    alignItems: 'center',
    left: 0,
    right: 0,
  },
  headerText: {
    color: '#000',
    fontSize: 15,
    textAlign: 'center',
  },
});
